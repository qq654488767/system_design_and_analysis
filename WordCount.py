import argparse
import os.path
import re

import wx
import wx.xrc

word_regex = re.compile('[A-Za-z][A-Za-z]+')
line_regex = re.compile('\\n')
valid_source_file_suffix_recedes = [re.compile(x) for x in [".*\.c", ".*\.cpp,", ".*\.java"]]
OUTPUT_FILENAME, CHARACTER_COUNT_RESULT, WORD_COUNT_RESULT, LINE_COUNT_RESULT, CODE_LINE_COUNT, BLANK_LINE_COUNT, COMMENT_LINE_COUNT = "output_filename", "character_count_result", "word_count_result", "line_count_result", "code_line_count", "blank_line_count", "comment_line_count"


class MyFrame1(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(301, 507), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.m_checkBox1 = wx.CheckBox(self, wx.ID_ANY, u"统计字符数", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_checkBox1, 0, wx.ALL, 5)

        self.m_checkBox2 = wx.CheckBox(self, wx.ID_ANY, u"统计单词数", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_checkBox2, 0, wx.ALL, 5)

        self.m_checkBox3 = wx.CheckBox(self, wx.ID_ANY, u"统计行数", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_checkBox3, 0, wx.ALL, 5)

        self.m_checkBox4 = wx.CheckBox(self, wx.ID_ANY, u"统计详细信息", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_checkBox4, 0, wx.ALL, 5)

        self.m_checkBox5 = wx.CheckBox(self, wx.ID_ANY, u"停用词", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_checkBox5, 0, wx.ALL, 5)

        self.m_textCtrl2 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_textCtrl2, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)

        self.m_filePicker1 = wx.FilePickerCtrl(self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*",
                                               wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE)
        bSizer1.Add(self.m_filePicker1, 0, wx.ALL, 5)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, u"输出文件名", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        bSizer1.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.m_textCtrl1 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_textCtrl1, 0, wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND, 5)

        self.m_button1 = wx.Button(self, wx.ID_ANY, u"确认", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.m_button1, 0, wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()

        self.Centre(wx.BOTH)
        self.Bind(wx.EVT_BUTTON, self.onButtonClicked)

    def __del__(self):
        pass

    def onButtonClicked(self, e):
        path = self.m_filePicker1.GetPath()
        if path == "":
            return
        result_dic = {}
        with open(path, "r", encoding='utf-8') as f:
            file_content = f.read()
        cur_file_result_dic = {OUTPUT_FILENAME: "result.txt"}
        if self.m_checkBox1:
            cur_file_result_dic[CHARACTER_COUNT_RESULT] = count_character(file_content)
        if self.m_checkBox2:
            cur_file_result_dic[WORD_COUNT_RESULT] = count_word(file_content, self.m_textCtrl2.GetValue())
        if self.m_checkBox3:
            cur_file_result_dic[LINE_COUNT_RESULT] = count_line(file_content)
        if self.m_textCtrl1.GetValue() != "":
            cur_file_result_dic[OUTPUT_FILENAME] = self.m_textCtrl1.GetValue()
        if self.m_checkBox4:
            cur_file_result_dic[CODE_LINE_COUNT] = count_code_line(file_content)
            cur_file_result_dic[BLANK_LINE_COUNT] = count_blank_line(file_content)
            cur_file_result_dic[COMMENT_LINE_COUNT] = count_comment_line(file_content)
        result_dic[path] = cur_file_result_dic
        write_to_file(result_dic)
        dlg = wx.MessageDialog(None, u"写入成功", u"标题信息", wx.YES_NO | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            pass
        dlg.Destroy()


def is_valid_file(arg):
    """to decide whether the input file is valid


    :param parser:current args parser
    :param arg:current arg
    """

    if not os.path.exists(arg):
        os.error("The file %s does not exist!" % arg)
    else:
        return open(arg, 'r', encoding="utf-8")  # return an open file handle


def count_character(content):
    return len(content)


def count_word(content, stop_word_path):
    words = re.findall(word_regex, content)
    stop_words = []
    if len(stop_word_path) > 0:
        try:
            with open(stop_word_path, 'r') as f:
                stop_words = f.read().split(" ")
        except FileNotFoundError:
            print("warning : stop word file not found!")

    return len([x for x in words if x not in stop_words])


def count_line(content):
    # lines = re.findall(line_regex, content)
    # in case there is only one line in file
    # return 0 if len(content) == 0 else len(lines) + 1
    return len(content.split('\n'))


def is_valid_file_name(name):
    for each_regex in valid_source_file_suffix_recedes:
        if re.findall(each_regex, name):
            return True
    return False


def write_to_file(result_dic, mode="w"):
    """write or append data to file

    :param result_dic: result dict
    :param mode: file process mode
    :return: none
    """
    # Cause I store output file path inside of each input filename dict,
    # so we have to go inside the dict to get output file path.
    # bad design of data structure leads to bad code.

    result_file_path = ""
    if result_dic is None:
        return
    for each_key in result_dic.keys():
        result_file_path = result_dic[each_key].get(OUTPUT_FILENAME)
        break
    if result_file_path == "" or result_file_path is None:
        return
    # if output file path is valid
    # start writing
    with open(result_file_path, mode, encoding="utf-8") as f:
        for each_key in result_dic.keys():
            # remove prefix
            f.write(each_key[len(os.getcwd()) + 1:] + ",")
            f.write("字符数," + str(result_dic[each_key].get(CHARACTER_COUNT_RESULT)) + ",") if result_dic[each_key].get(
                CHARACTER_COUNT_RESULT) is not None else None
            f.write("单词数," + str(result_dic[each_key].get(WORD_COUNT_RESULT)) + ",") if result_dic[each_key].get(
                WORD_COUNT_RESULT) is not None else None
            f.write("行数," + str(result_dic[each_key].get(LINE_COUNT_RESULT)) + ",") if result_dic[each_key].get(
                LINE_COUNT_RESULT) is not None else None
            f.write("代码行数," + str(result_dic[each_key].get(CODE_LINE_COUNT)) + ",") if result_dic[each_key].get(
                COMMENT_LINE_COUNT) is not None else None
            f.write("注释行数," + str(result_dic[each_key].get(COMMENT_LINE_COUNT)) + ",") if result_dic[each_key].get(
                COMMENT_LINE_COUNT) is not None else None
            f.write("空白行数," + str(result_dic[each_key].get(BLANK_LINE_COUNT)) + ",") if result_dic[each_key].get(
                BLANK_LINE_COUNT) is not None else None
            f.write("\n")


def get_file_recursively(filepath):
    files = os.listdir(filepath)
    for fi in files:
        fi_d = os.path.join(filepath, fi)
        if os.path.isdir(fi_d):
            get_file_recursively(fi_d)
        else:
            yield os.path.join(filepath, fi_d)


def count_code_line(file_content):
    lines = file_content.split("\n")
    counter = 0
    stack = []
    for line in lines:
        if line == "":
            continue
        if "//" in line:
            continue
        if "/*" in line:
            stack.append(line)
        if "*/" in line:
            stack.pop()
            continue
        if len(stack) > 0:
            continue
        else:
            counter += 1
    return counter


def count_blank_line(file_content):
    lines = file_content.split("\n")
    counter = 0
    stack = []
    for line in lines:
        if "//" in line:
            continue
        if "/*" in line:
            stack.append(line)
        if "*/" in line:
            stack.pop()
            continue
        if len(stack) > 0:
            continue
        if len(line) > 0:
            continue
        if line == "":
            counter += 1
    return counter


def count_comment_line(file_content):
    lines = file_content.split("\n")
    counter = 0
    stack = []
    for line in lines:
        if "*/" in line:
            counter += 1
            stack.pop()
            continue
        if len(stack) > 0:
            counter += 1
            continue
        if "/*" in line:
            counter += 1
            stack.append(line)
            continue
        if "//" in line:
            counter += 1
            continue
    return counter


def handle_parameters(args):
    """do different works according to the result of args

    :param args: the parsed argument dict
    :return: dict
    """
    # check if input filename type is file we can handle
    if not is_valid_file_name(args.infile):
        print("error:{} is not a valid file name!".format(args.infile))
        return

    result_dic = {}

    # if -x is inside command line option, exit after finishing
    if args.interface:
        app = wx.App()
        frm = MyFrame1(None)
        frm.Show()
        app.MainLoop()
        return

    # if we need to handle valid files recursively, we should void checking input filename,
    # for example, *.cpp, we can open this file, we should get it suffix instead.
    if args.recursive:
        # get each filename and check whether it's the file we should handle.
        for each_file in list(get_file_recursively(os.getcwd())):
            if not is_valid_file_name(each_file):
                continue
            # split filename to compare it's suffix
            if not each_file.split(".")[1] == args.infile.split(".")[1]:
                continue
            # set default output filename
            cur_file_result_dic = {OUTPUT_FILENAME: "result.txt"}
            # read file content
            with open(each_file, 'r', encoding="utf-8") as f:
                file_content = f.read()
            # args is a dict itself, and all the actions have been set to store_true,
            # so we can just get this item to check whether it's true and do the corresponding function.
            if args.character:
                cur_file_result_dic[CHARACTER_COUNT_RESULT] = count_character(file_content)
            if args.word:
                cur_file_result_dic[WORD_COUNT_RESULT] = count_word(file_content, args.e)
            if args.line:
                cur_file_result_dic[LINE_COUNT_RESULT] = count_line(file_content)
            if args.output:
                cur_file_result_dic[OUTPUT_FILENAME] = args.output
            if args.all:
                cur_file_result_dic[CODE_LINE_COUNT] = count_code_line(file_content)
                cur_file_result_dic[BLANK_LINE_COUNT] = count_blank_line(file_content)
                cur_file_result_dic[COMMENT_LINE_COUNT] = count_comment_line(file_content)
            # record to result_dic of each file
            result_dic[each_file] = cur_file_result_dic
    # if not recursive mode
    else:
        # same process above
        cur_file_result_dic = {OUTPUT_FILENAME: "result.txt"}
        file_content = is_valid_file(args.infile).read()
        if args.character:
            cur_file_result_dic[CHARACTER_COUNT_RESULT] = count_character(file_content)
        if args.word:
            cur_file_result_dic[WORD_COUNT_RESULT] = count_word(file_content, args.e)
        if args.line:
            cur_file_result_dic[LINE_COUNT_RESULT] = count_line(file_content)
        if args.output:
            cur_file_result_dic[OUTPUT_FILENAME] = args.output
        if args.all:
            cur_file_result_dic[CODE_LINE_COUNT] = count_code_line(file_content)
            cur_file_result_dic[BLANK_LINE_COUNT] = count_blank_line(file_content)
            cur_file_result_dic[COMMENT_LINE_COUNT] = count_comment_line(file_content)
        # os.getcwd is to keep the same format of input files
        # so that we can handle it same way in write to file function.
        result_dic[os.getcwd() + os.pathsep + args.infile] = cur_file_result_dic

    return result_dic


def parse_args(parser):
    """ parse command line input

    :param parser: current argument parser
    :return: arg
    :type: dict
    """

    # add command arguments, -c,-w,-l...
    parser.add_argument("-c", "--character", action="store_true", help="show the amount of characters")
    parser.add_argument("-w", "--word", action="store_true", help="show the amount of words")
    parser.add_argument("-l", "--line", action="store_true", help="show the amount of lines")
    parser.add_argument("-s", "--recursive", action="store_true", help="process files in current directory recursively")
    parser.add_argument("-a", "--all", action="store_true",
                        help="count detailed data that includes amount of code line, blank line, comment line")
    parser.add_argument("-e", nargs="?", default="stopword.txt",
                        help="count words without stop words in given name")
    parser.add_argument("infile")
    parser.add_argument("-o", "--output")
    parser.add_argument("-x", "--interface", action="store_true", help="show the interface of this program")
    # here does all the processing work
    args = parser.parse_args()

    return args


def main():
    parser = argparse.ArgumentParser()

    args = parse_args(parser)

    result_dic = handle_parameters(args)

    write_to_file(result_dic)


if __name__ == '__main__':
    main()
